<?php

namespace Drupal\xtcelastica\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;
use Drupal\xtcsearch\Form\Traits\FilterSearchTrait;
use Drupal\xtcsearch\Form\Traits\PaginationTrait;
use Drupal\xtcsearch\Form\Traits\QueryTrait;
use Elastica\Document;
use Elastica\Exception\InvalidException;

/**
 * Plugin implementation of the xtc_handler for File.
 *
 */
abstract class ElasticaBase extends XtcHandlerPluginBase {

  const TIMEOUT = 5;

  /**
   * @var array
   */
  protected $definition;

  /**
   * @var array
   */
  protected $server = [];

  /**
   * @var array
   */
  protected $request = [];

  use FilterSearchTrait;
  use QueryTrait;
  use PaginationTrait;

  public function processFilters() {
    return $this->process()
      ->filters();
  }

  protected function filters() {
    foreach ($this->filters as $name => $container) {
      $filter = $this->loadFilter($name);
      if (!empty($this->resultSet)) {
        try {
          $agg = $this->resultSet->getAggregation($filter->getFieldName());
        } catch (InvalidException $e) {
        } finally {
        }
      }

      if (!empty($agg['buckets'])) {
        foreach ($agg['buckets'] as $option) {
          $options[$option['key']] = $option['key'] . ' (' . $option['doc_count'] . ')';
        }
      }
      $facets[$name] = $options;

    }
    return $facets;
  }

  protected function initProcess() {
    $this->definition = $this->profile;
  }

  protected function runProcess() {
    $this->initFilters();
    $this->initElastica();
    $this->initPagination();
    $this->initQuery();
    $this->setCriteria();
    $this->getResultSet();
  }

  protected function adaptContent() {
    $response = $this->resultSet->getResponse()->getData();
    $this->content['values'] = $response['hits']['hits'];
    $this->content['pagination'] = $this->pagination;
  }

  protected function getTimeout() {
    return self::TIMEOUT;
  }


}
