<?php

namespace Drupal\xtcelastica\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elastica_get",
 *   label = @Translation("Elastica Search for XTC"),
 *   description = @Translation("Elastica Search for XTC description.")
 * )
 */
class ElasticaGet extends ElasticaBase {


}
